/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rotleft;

/**
 *
 * @author Bernard
 */
public class RotLeft {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 4, 5, 6, 7};
        int randomNumber = 8;
        int returned_arr[] = rotLeft(arr, randomNumber);

        for (int i = 0; i < returned_arr.length; i++) {
            System.out.print(returned_arr[i] + " ");
        }
    }

    private static int[] rotLeft(int[] a, int d) {
        try {

            for (int i = 0; i < d; i++) {
                int j, temp;
                temp = a[0];
                //Rotate array by one
                for (j = 0; j < a.length - 1; j++) {
                    a[j] = a[j + 1];
                }
                a[j] = temp;
            }

        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return a;
    }

}
