/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scratchcards;

import httpserver.ScratchCardHttpServer;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Bernard
 */
public class ScratchCards {

    public static HashMap<String, String> sysprop = new HashMap();
    public static ExecutorService executorThread;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         Properties prop = new Properties();

        try {
            readPropertyFile();
            if (!sysprop.isEmpty()) {
                int MAX_THREADS = Integer.parseInt(sysprop.get("MAX_THREADS"));
                executorThread = Executors.newFixedThreadPool(MAX_THREADS);
                startHTTPServer();
            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
        }
    }
    
     public static void readPropertyFile() {
        System.out.println("Finding configurations ...");
        Properties prop = new Properties();
        try {
            FileInputStream fip = new FileInputStream("config.properties");
            prop.load(fip);
            System.out.println("Property File Loaded Succesfully");
            Set<String> propertyNames = prop.stringPropertyNames();
            for (String Property : propertyNames) {
                sysprop.put(Property, prop.getProperty(Property));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void LogProcessor() {
        Timer repeat = new Timer();
       // repeat.schedule(new LogWriter(), 5, 1000);
    }
     public static void startHTTPServer() {
        try {
            ScratchCardHttpServer.init(Integer.parseInt(sysprop.get("EXCHANGE_SERVER_PORT")), sysprop.get("EXCHANGE_SERVER_CONTEXT"));//Connection to HTTP Exchange
            System.out.println("SCRATCH CARD SERVER STARTED LISTENING ON PORT..." + sysprop.get("EXCHANGE_SERVER_PORT"));
        } catch (Exception ex) {
            System.out.println("An Error Occured when starting HTTP Server on port " + sysprop.get("EXCHANGE_SERVER_PORT"));
            System.exit(0); //Kill this instance
        }
    }
}
