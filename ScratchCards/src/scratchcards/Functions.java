/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scratchcards;

import database.DBFunctions;
import java.security.SecureRandom;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Bernard
 */
public class Functions {

    String response = "";

    private JSONObject reqdata = null;
    JSONObject resjson = new JSONObject();

    DBFunctions dbf = new DBFunctions();

    public Functions(JSONObject reqdata) {
        this.reqdata = reqdata;
    }

    /**
     * Function to cre
     * 
     * @return 
     */
    public String CreateRecord() {
        JSONObject mainObj = new JSONObject();
        try {
            SecureRandom random = new SecureRandom();
            long randomlong = random.nextLong();
            int randomint = random.nextInt();
            String vourcher_no = Math.abs(randomlong) + "" + Math.abs(randomint);
            String amount = reqdata.has("AMOUNT") ? reqdata.getString("AMOUNT") : "";

            if (amount.equalsIgnoreCase("")) {
                //Return error message
            } else {
                if (dbf.CreateCardRecord(vourcher_no, amount)) {
                    mainObj.put("statuscode", "00");
                    mainObj.put("message", "Voucher created successfully");
                    mainObj.put("operation", "CREATE_CARD");
                } else {
                    mainObj.put("statuscode", "57");
                    mainObj.put("message", "Failed to create voucher");
                    mainObj.put("operation", "CREATE_CARD");

                }
            }
        } catch (JSONException exc) {
            exc.printStackTrace();
            mainObj.put("statuscode", "57");
            mainObj.put("message", "Failed to update inactive records");
            mainObj.put("operation", "CREATE_CARD");
        }
        return mainObj.toString();
    }

    /**
     * Function to return card number for a given serail number
     *
     * @return 
     */
    public String RequestCard() {
        try {
             String serial_no = reqdata.has("SERIAL_NO") ? reqdata.getString("SERIAL_NO") : "";
            String card_details = dbf.getCardRecordDetailsBySerialNo(serial_no);
            JSONObject json_result = new JSONObject(card_details);
            String status_code = json_result.getString("statuscode");
            if (status_code.equalsIgnoreCase("00")) {
                //return card deteails
                response = card_details;
            } else {
                //return error response
                JSONObject mainObj = new JSONObject();;
                mainObj.put("statuscode", "57");
                mainObj.put("message", "Invalid scratch card request");
                mainObj.put("operation", "REQUEST_CARD");
                response = mainObj.toString();
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        return response;
    }

    public String getRecordsInsertedLastThreeMinutes() {
        return dbf.getCardDetails();
    }

    //Get card records from serial number
    public String fetchCardRecords() {
        String serial_no = reqdata.has("SERIAL_NO") ? reqdata.getString("SERIAL_NO") : "";
        return dbf.getCardRecordDetailsBySerialNo(serial_no);
    }

    public String ChangeStatustoIncative() {
        JSONObject mainObj = new JSONObject();
        if (dbf.ChangeStatusToInacttive()) {
            mainObj.put("statuscode", "00");
            mainObj.put("message", "All inactive records updated successfully");
            mainObj.put("operation", "CHANGE_STATUS");
        } else {
            mainObj.put("statuscode", "57");
            mainObj.put("message", "Failed to update inactive records");
            mainObj.put("operation", "CHANGE_STATUS");

        }
        return mainObj.toString();
    }
}
