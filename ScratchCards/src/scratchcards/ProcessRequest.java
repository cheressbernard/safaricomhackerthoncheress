/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scratchcards;

import org.json.JSONObject;

/**
 *
 * @author Bernard
 */
public class ProcessRequest {

    public String processTransaction(String request, String RemoteAddress) {
        JSONObject mainObj = new JSONObject();
        String response = "";
        System.out.println(request);
        try {
            if (IsValidJSON(request)) {
                JSONObject request_json = new JSONObject(request);
                String operation = request_json.has("OPERATION") ? request_json.get("OPERATION").toString() : "";
                switch (operation) {
                    case "CHANGE_STATUS":
                        Functions change_status= new Functions(request_json);
                        response=change_status.ChangeStatustoIncative();
                        break;
                    case "REQUEST_CARD":
                        Functions reqCard= new Functions(request_json);
                        response=reqCard.RequestCard();
                        break;
                    case "FETCH_LAST_RECORDS":
                        Functions fetchCardRec= new Functions(request_json);
                        response=fetchCardRec.getRecordsInsertedLastThreeMinutes();
                        break;
                    case "FETCH_CARD_DETAILS":
                        Functions fetchCard= new Functions(request_json);
                        response=fetchCard.fetchCardRecords();
                        break;
                    case "CREATE_CARD":
                        Functions create_card= new Functions(request_json);
                        response=create_card.CreateRecord();
                        break;
                    default:
                        mainObj.put("statuscode", "57");
                        mainObj.put("message", "Invalid operation request");
                        response = mainObj.toString();
                        break;
                }
            } else {
                mainObj.put("statuscode", "57");
                mainObj.put("message", "Error occured in backend, not valid JSON");
                response = mainObj.toString();
            }
        } catch (Exception e) {
            mainObj.put("statuscode", "57");
            mainObj.put("message", "Error occured in backend");
            response = mainObj.toString();

        }
        return response;
    }

    private boolean IsValidJSON(String json_request) {
        try {
            JSONObject new_json = new JSONObject(json_request);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
