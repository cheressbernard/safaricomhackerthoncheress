/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 *
 * @author Bernard
 */
public class SimpleHttpPoster {
    HashMap xmlmap = new HashMap<String, String>();
   
    public String httpPoster(String respMsg, String endPoint) {
        String reply = "no response";
        try {
            URL url = new URL(endPoint);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            // set up url connection to get retrieve information back
            con.setRequestMethod("POST");
            con.setDoInput(true);
            con.setDoOutput(true);
            // stuff the Authorization request header
            con.setRequestProperty("Connection", "close");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Content-Length", String.valueOf(respMsg.getBytes("UTF-8").length));
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            OutputStream out = con.getOutputStream();
            out.write(respMsg.getBytes("UTF-8"));
            out.close();

            // pull the information back from the URL
            InputStream is = null;
            if (con.getResponseCode() == 200) {
                is = con.getInputStream();
            } else {
                is = con.getErrorStream();
            }
            reply = getStringFromInputStream(is);

        } catch (Exception ex) {

        }
        return reply;
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception ex) {

        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {

                }
            }
        }
        return sb.toString();
    }
}
