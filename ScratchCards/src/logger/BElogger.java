/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SocketHandler;
import static scratchcards.ScratchCards.sysprop;

/**
 *
 * @author Bernard
 */
public class BElogger {
    private FileHandler fh = null;
    private String pathtologs = "";

    //for socket handler
    private SocketHandler sh = null;
  
    private String logsfiledirectory = "Safaricom";

    private String logsfilename = null;
    private String msg = null;
    private static Logger logger = null;
    public HashMap<String, String> logmap;
    
    /**
     * 
     * @param filename
     * @param msg 
     */
    public BElogger(String filename, String msg) {
        this.logsfilename = filename;
        this.msg = msg;
        this.pathtologs = sysprop.get("CARDSLOGSPATH");
        this.logsfiledirectory = sysprop.get("CARDSLOGS");
    }
    
    /**
     * 
     * @param msg String message to be logged without
     */    
    public BElogger(String msg) {
        this.logsfilename = "exceptions";
        this.msg = msg;
        this.pathtologs = sysprop.get("CARDSLOGS");
        this.logsfiledirectory = sysprop.get("CARDSLOGS");
    }
    
    /**
     * 
     *Log the file
     */
    public void log() {
        logger = Logger.getLogger("");
        try {
            fh = new FileHandler(createDailyDirectory() + logsfilename + ".txt", 26000000, 20, true);
            fh.setFormatter(new LogsFormatter());
            logger.addHandler(fh);
            logger.setLevel(Level.FINE);
            logger.fine(msg);
            fh.close();
        } catch (IOException | SecurityException e) {
            }

    }
    
    public String createDailyDirectory() {
        String Dailydirectory;
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
        String daylog = format.format(new Date());
        Dailydirectory = pathtologs + daylog;
        new File(Dailydirectory).mkdirs();
        return Dailydirectory + "/";
    }
}
