/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import org.json.JSONArray;
import org.json.JSONObject;
import static scratchcards.ScratchCards.sysprop;

/**
 *
 * @author Bernard
 */
public class DBFunctions {

    public DBConn DBConn;

    public void getDBConn() {
        this.DBConn = new DBConn();
    }

    
    public boolean CreateCardRecord(String vourcher_no, String amount) {
        boolean created = false;
        PreparedStatement upd = null;
        getDBConn();
        try {
            String expiry_minutes = sysprop.get("EXPIRY_TIME");;
            String strInMsg = "INSERT INTO  simcards (VOURCHERNO,EXPIRYDATE ,AMOUNT,STATUS) VALUES (?,DATE_ADD(CURRENT_TIMESTAMP , INTERVAL " + expiry_minutes + " MINUTE),?,?)";
            upd = DBConn.conn.prepareStatement(strInMsg);
            upd.setString(1, vourcher_no);
           // upd.setString(2, "DATE_ADD(CURRENT_TIMESTAMP , INTERVAL " + expiry_minutes + " MINUTE)");
            upd.setString(2, amount);
            upd.setString(3, "ACTIVE");
            upd.executeUpdate();
            created = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return created;
    }

    public String getCardDetails() {
        String Result = "";
        JSONObject mainObj = new JSONObject();
        getDBConn();
        JSONArray ja = new JSONArray();
        try {
            String sql = "SELECT VOURCHERNO,SERIALNO,EXPIRYDATE,AMOUNT,STATUS, DATECREATED , DATEUPDATED  FROM simcards WHERE DATECREATED>DATE_ADD(CURRENT_TIMESTAMP , INTERVAL -3 MINUTE)";
            DBConn.stmt.executeQuery(sql);
            DBConn.rs = DBConn.stmt.getResultSet();
            JSONObject jo = new JSONObject();

            int recordsFetched = 0;
            while (DBConn.rs.next()) {
                ResultSetMetaData rsmd = DBConn.rs.getMetaData();
                int columnCount = rsmd.getColumnCount();
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = rsmd.getColumnName(i);
                    String columnValue = DBConn.rs.getString(columnName);
                    columnValue = columnValue == null ? "" : columnValue;  //JSON doesn't allow pupulating Null values
                    jo.put(columnName, columnValue);
                }
                ja.put(jo);
                jo = new JSONObject(); //Just to clear the object key values
                recordsFetched += 1;
            }
            mainObj.put("statuscode", recordsFetched != 0 ? "00" : "57");
            mainObj.put("message", recordsFetched != 0 ? "Data retrieval successful" : "No data retrieved");
            mainObj.put("operation", "FETCH_CARD_RECORDS");
            mainObj.put("params", ja);
            Result = mainObj.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            mainObj.put("statuscode", "57");
            mainObj.put("message", "Card Details retrieval failed");
            mainObj.put("operation", "FETCH_CARD_RECORDS");
            mainObj.put("params", ja);
            Result = mainObj.toString();
        } finally {
            DBConn.close();
        }

        return Result;
    }
    
    public String getCardRecordDetailsBySerialNo(String serial_no) {
        String Result = "";
        JSONObject mainObj = new JSONObject();
        getDBConn();
        JSONArray ja = new JSONArray();
        try {
            String sql = "SELECT VOURCHERNO,SERIALNO,EXPIRYDATE,AMOUNT,STATUS, DATECREATED , DATEUPDATED  FROM simcards WHERE SERIALNO='"+serial_no+"'";
            DBConn.stmt.executeQuery(sql);
            DBConn.rs = DBConn.stmt.getResultSet();
            JSONObject jo = new JSONObject();

            int recordsFetched = 0;
            while (DBConn.rs.next()) {
                ResultSetMetaData rsmd = DBConn.rs.getMetaData();
                int columnCount = rsmd.getColumnCount();
                for (int i = 1; i <= columnCount; i++) {
                    String columnName = rsmd.getColumnName(i);
                    String columnValue = DBConn.rs.getString(columnName);
                    columnValue = columnValue == null ? "" : columnValue;  //JSON doesn't allow pupulating Null values
                    jo.put(columnName, columnValue);
                }
                ja.put(jo);
                jo = new JSONObject(); //Just to clear the object key values
                recordsFetched += 1;
            }
            mainObj.put("statuscode", recordsFetched != 0 ? "00" : "57");
            mainObj.put("message", recordsFetched != 0 ? "Data retrieval successful" : "No data retrieved");
            mainObj.put("operation", "FETCH_CARD_DETAILS");
            mainObj.put("params", ja);
            Result = mainObj.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            mainObj.put("statuscode", "57");
            mainObj.put("message", "Card Details retrieval failed");
            mainObj.put("operation", "FETCH_CARD_RECORDS");
            mainObj.put("params", ja);
            Result = mainObj.toString();
        } finally {
            DBConn.close();
        }

        return Result;
    }
    
    public boolean ChangeStatusToInacttive(){
        boolean changed=false;
        PreparedStatement upd = null;
        getDBConn();
        try {
            String expiry_minutes=sysprop.get("EXPIRY_TIME");
            String strInMsg = "UPDATE  simcards SET STATUS=?, DATEUPDATED=NOW() WHERE DATECREATED=DATE_ADD(CURRENT_TIMESTAMP , INTERVAL -" + expiry_minutes + " MINUTE)";
            upd = DBConn.conn.prepareStatement(strInMsg);
            upd.setString(1, "INACTIVE");
            upd.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConn.close();
        }
        return changed;
    }
    
    public boolean DeleteInactiveRecords(){
        boolean changed=false;
        PreparedStatement upd = null;
        getDBConn();
        try {
            String expiry_minutes=sysprop.get("EXPIRY_TIME");
            String strInMsg = "UPDATE  simcards SET DELETED=?, DATEUPDATED=NOW() WHERE STATUS='INACTIVE'";
            upd = DBConn.conn.prepareStatement(strInMsg);
            upd.setInt(1, 1);
            upd.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DBConn.close();
        }
        return changed;
    }
}
