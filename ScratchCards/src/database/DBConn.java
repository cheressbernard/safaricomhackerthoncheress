/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static scratchcards.ScratchCards.sysprop;

/**
 *
 * @author Bernard
 */
public class DBConn {
     public Connection conn = null;
    public Statement stmt = null;
    public PreparedStatement preparedstmt = null;
    public ResultSet rs = null;

    public DBConn() {
        String ip = sysprop.get("MACHINE");
        String port=sysprop.get("DBPORT");
        String db_name=sysprop.get("DBNAME");
        String username=sysprop.get("DBUSER");
        String password=sysprop.get("DBPASSWORD");
      
      
      try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/scratchcards", "root", "");
//            this.conn = DriverManager.getConnection("jdbc:mysql://"+ip+":"+port+"/"+db_name, username, password);
            this.stmt = this.conn.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
//            ESBlogger el = new ESBlogger(sw.toString());
//            el.log();
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            //Close JDBC objects as soon as possible
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
            if (conn != null) {
                conn = null;
            }
            if (preparedstmt != null) {
                preparedstmt = null;
            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
//            ESBLog el = new ESBLog(sw.toString());
//            el.log();
        }
    }
}
