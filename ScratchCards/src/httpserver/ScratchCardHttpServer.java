/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httpserver;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.Executors;
import scratchcards.ProcessRequest;
import static scratchcards.ScratchCards.sysprop;
import scratchcards.UpdateInactiveRecords;

/**
 *
 * @author Bernard
 */
public class ScratchCardHttpServer {

    public static int MAX_THREADS;

    public static void init(int port, String context) throws Exception {
         (new Timer()).schedule(new UpdateInactiveRecords(), 0, (1000 * 10 * 1));
        MAX_THREADS = Integer.parseInt(sysprop.get("MAX_THREADS"));
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext(context, new MyHandler());
        server.setExecutor(Executors.newFixedThreadPool(MAX_THREADS));
        server.start();
    }

    static class MyHandler implements HttpHandler {

        public void handle(HttpExchange t) throws IOException {
            String request = null, response = null, line = null, RemoteAddress = null;
            HashMap responseBuff = new HashMap();

            Gson gson = new Gson();
            try {
                Headers reqHeaders = t.getRequestHeaders();
                String header_token = "";
                String request_ID = "";
                for (Map.Entry<String, List<String>> he : reqHeaders.entrySet()) {
//                    System.out.println(he.getKey());

                    for (String v : he.getValue()) {
                        // System.out.println("\t" + v);
                        if (he.getKey().equalsIgnoreCase("token")) {
                            header_token = v;
                        }
                        if (he.getKey().equalsIgnoreCase("RequestID")) {
                            request_ID = v;
                        }
                    }
                }
                InputStream in = t.getRequestBody();
                StringBuilder jb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((line = reader.readLine()) != null) {
                    jb.append(line);
                }
                reader.close();
                request = jb.toString();
                RemoteAddress = t.getRemoteAddress().getHostString();

                if ("GET".equalsIgnoreCase(t.getRequestMethod())) {
                    responseBuff.put("STATUS", "CONNECTED");
                    response = gson.toJson(responseBuff);
                } else if ("POST".equalsIgnoreCase(t.getRequestMethod())) {
                    ProcessRequest http = new ProcessRequest();
                    String responseBuffer = http.processTransaction(request, RemoteAddress);
                    response = responseBuffer;

                } else {
                    responseBuff.put("91", "PROTOCOL NOT SUPPORTED");
                    response = gson.toJson(responseBuff);
                }

                OutputStream os = t.getResponseBody();
                t.sendResponseHeaders(200, response.length());
                os.write(response.getBytes());
                os.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
