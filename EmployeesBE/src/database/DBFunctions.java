/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.PreparedStatement;

/**
 *
 * @author Bernard
 */
public class DBFunctions {

    public DBConn DBConn;

    public void getDBConn() {
        this.DBConn = new DBConn();
    }
    public boolean CreateEmployee(String id, String employee_name, String employee_age, String employee_salary, String profile_image) {
        boolean created = false;
        PreparedStatement upd = null;
        getDBConn();
        try {
            String strInMsg = "INSERT INTO  employees (id,employee_name ,employee_age,employee_salary, profile_image) VALUES (?,?,?,?,?)";
            upd = DBConn.conn.prepareStatement(strInMsg);
            upd.setString(1, id);
            upd.setString(2, employee_name);
            upd.setString(3, employee_age);
            upd.setString(4, employee_salary);
            upd.setString(4, profile_image);
            upd.executeUpdate();
            created = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return created;
    }
    
}
