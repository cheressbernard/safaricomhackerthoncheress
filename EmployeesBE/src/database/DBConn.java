/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Bernard
 */
public class DBConn {
    public Connection conn = null;
    public Statement stmt = null;
    public ResultSet rs = null;

    public DBConn() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/scratchcards", "root", "");
            this.stmt = this.conn.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
//            ESBlogger el = new ESBlogger(sw.toString());
//            el.log();
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            //Close JDBC objects as soon as possible
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
            //Close context
        } catch (SQLException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
//            ESBLogger el = new ESBLogger(sw.toString());
//            el.log();
        }
    }
}
