/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httpposter;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import static java.util.Base64.getEncoder;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Bernard
 */
public class AESEncryption {
    private String iv = "_VSUrIqGV2pHSye1";
    private String secretkey = "da0k188qL5OiY3eX";
    private IvParameterSpec ivspec;
    private SecretKeySpec keyspec;
    private Cipher cipher;
    
    public AESEncryption(){
        ivspec = new IvParameterSpec(iv.getBytes());
        keyspec = new SecretKeySpec(secretkey.getBytes(), "AES");

        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
//            ESBLog ESBLogger = new ESBLog(sw.toString());
//            ESBLogger.log();
        } catch (NoSuchPaddingException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
//            ESBLog ESBLogger = new ESBLog(sw.toString());
//            ESBLogger.log();
        }
    }
    HashMap resultMap = new HashMap();
    public static final String AES_KEY = "NUiGKv97lfHAeiRc";
    public static final String AES_PADDING = "AES";
     public static String AESencrypt(String input) throws Exception {
        byte[] crypted = null;
        try {
            SecretKeySpec skey = new SecretKeySpec(AES_KEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance(AES_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, skey);
            crypted = cipher.doFinal(input.getBytes());
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
        }
        return new String(getEncoder().encode(crypted));
    }

    public HashMap AESdecrypt(String encrypted) {
        String result = null;
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(AES_KEY.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] base64_String = Base64.decodeBase64(encrypted.getBytes());
            byte[] original = cipher.doFinal(base64_String);
            result = new String(original);
            resultMap.put("39", "00");
            resultMap.put("48", result);
        } catch (Exception e) {
            resultMap.put("39", "06");
            resultMap.put("48", "EXCEPTION ERROR | " + result);
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            //Functions func = new Functions();
//            func.log(sw.toString(), "exception");
        }
        return resultMap;
    }
    
    public byte[] encrypt(String text) throws Exception {
        if (text == null || text.length() == 0) {
            throw new Exception("Empty string");
        }
        byte[] encrypted = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            encrypted = cipher.doFinal(text.getBytes("UTF-8"));
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
//            ESBLog ESBLogger = new ESBLog(sw.toString());
//            ESBLogger.log();
        }
        return encrypted;
    }

    public byte[] decrypt(String code) throws Exception {
        if (code == null || code.length() == 0) {
            throw new Exception("Empty string");
        }
        byte[] decrypted = null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
            decrypted = cipher.doFinal(hexToBytes(code));
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
//            ESBLog ESBLogger = new ESBLog(sw.toString());
//            ESBLogger.log();
        }
        return decrypted;
    }

    public String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        }
        int len = data.length;
        String str = "";
        for (int i = 0; i < len; i++) {
            if ((data[i] & 0xFF) < 16) {
                str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
            } else {
                str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
            }
        }
        return str;
    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16);
            }
            return buffer;
        }
    }
    
    public String hmacDigest(String msg) {
        String digest = null;
        String keyString = "Eclectics!StheB00m";
        try {//sha256
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return digest;
    }
}
