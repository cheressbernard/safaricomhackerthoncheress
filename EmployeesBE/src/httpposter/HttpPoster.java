/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httpposter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import logger.BELogger;

/**
 *
 * @author Bernard
 */
public class HttpPoster {

    String urlParameters = "", result = "";
    URLConnection conn;
    private String[] exludedCipherSuites = {"_DHE_", "_DH_"};

    private TrustManagerFactory tmf;

    public void setExludedCipherSuites(String[] exludedCipherSuites) {
        this.exludedCipherSuites = exludedCipherSuites;
    }

    public String getEmployeeRecords() {
        String response = "";
        try {
            String url = "http://dummy.restapiexample.com/api/v1/employees";
            StringBuilder stringBuilder = new StringBuilder(url);
            stringBuilder.append(URLEncoder.encode("", "UTF-8"));
            URL obj = new URL(stringBuilder.toString());

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept-Charset", "UTF-8");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String line;
            StringBuffer response_ = new StringBuffer();

            while ((line = in.readLine()) != null) {
                response_.append(line);
            }
            in.close();
            response = response_.toString();
            BELogger logger = new BELogger("Employees_response", response);
            logger.logToJsonFile();
//            System.out.println(response_.toString());
        } catch (Exception e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            BELogger el = new BELogger(sw.toString());//&quot;
            el.log();
            response = e.getMessage();
        }
        return response;
    }

    /**
     * Function to create employee record with below parameters
     *
     * @param id
     * @param employee_name
     * @param employee_salary
     * @param employee_age
     * @param profile_image
     * @return
     */
    public String CreteEmployee(String id, String employee_name, String employee_salary, String employee_age, String profile_image) {

        try {
            String line;

            String json_request = "{\"name\":\"" + employee_name + "\",\"salary\":\"" + employee_salary + "\",\"age\":\"" + employee_age + "\"}";
            String reply = "no response";
            String endPoint = "http://dummy.restapiexample.com/api/v1/create";
            try {
                URL url = new URL(endPoint);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                // set up url connection to get retrieve information back
                con.setRequestMethod("POST");
                con.setDoInput(true);
                con.setDoOutput(true);
                // stuff the Authorization request header
                con.setRequestProperty("Connection", "close");
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Content-Length", String.valueOf(json_request.getBytes("UTF-8").length));
                con.setConnectTimeout(5000);
                con.setReadTimeout(5000);
                OutputStream out = con.getOutputStream();
                out.write(json_request.getBytes("UTF-8"));
                out.close();

                // pull the information back from the URL
                InputStream is = null;
                if (con.getResponseCode() == 200) {
                    is = con.getInputStream();
                } else {
                    is = con.getErrorStream();
                }
                reply = getStringFromInputStream(is);

            } catch (Exception ex) {
                reply = ex.getMessage();
            }
            return reply;

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            BELogger lg = new BELogger(sw.toString());
            lg.log();
        }
        return result;
    }

    public String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception ex) {

        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {

                }
            }
        }
        return sb.toString();
    }

    public String UpdateRecord(String id, String name, String age, String salary) {
        try {
            String json_request="{\"name\":\""+name+"\",\"salary\":\""+salary+"\",\"age\":\""+age+"\"}";
            URL url = new URL("http://dummy.restapiexample.com/api/v1/update/" + id);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(
                    httpCon.getOutputStream());
            out.write(json_request);
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
            out.close();
            reader.close();
            BELogger logger = new BELogger("update-response", result);
            logger.logToJsonFile();
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            BELogger el = new BELogger(sw.toString());//&quot;
            el.log();
            result = e.getMessage();
        }
        return result;
    }

    public String HttpPost(String data, String url, String SOAPAction) {
        StringBuffer stringBuffer = new StringBuffer();
        String response = "";
        try {
            SSLContext context = SSLContext.getInstance("TLS");
            TrustManager[] certs = new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String t) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String t) {
                    }
                }
            };
            context.init(null, certs, null);
            SSLParameters params = context.getSupportedSSLParameters();
            List<String> enabledCiphers = new ArrayList<String>();
            for (String cipher : params.getCipherSuites()) {
                boolean exclude = false;
                if (exludedCipherSuites != null) {
                    for (int i = 0; i < exludedCipherSuites.length && !exclude; i++) {
                        exclude = cipher.indexOf(exludedCipherSuites[i]) >= 0;
                    }
                }
                if (!exclude) {
                    enabledCiphers.add(cipher);
                }
            }
            String[] cArray = new String[enabledCiphers.size()];
            enabledCiphers.toArray(cArray);
            URL oURL = new URL(url);
            HttpsURLConnection con
                    = (HttpsURLConnection) oURL.openConnection();
            SSLSocketFactory sf = context.getSocketFactory();
            sf = new DOSSLSocketFactory(sf, cArray);
            con.setSSLSocketFactory(sf);
            con.setDoOutput(true);
            // con.setDoOutput(true);
            con.setRequestProperty("Content-type", "text/xml; charset=utf-8");
            con.setRequestProperty("SOAPAction", SOAPAction);
            con.setRequestMethod("POST");
            con.setDoOutput(true);

            con.setConnectTimeout(15000);
            con.setReadTimeout(45000);
            OutputStream reqStream = con.getOutputStream();
            reqStream.write(data.getBytes());

            int responseCode = con.getResponseCode();
            String inputLine;
            BufferedReader bufferedReader = null;
            if (responseCode == 200) {
                InputStream resStream = con.getInputStream();
                InputStreamReader streamReader = new InputStreamReader(resStream);
                bufferedReader = new BufferedReader(streamReader);
                while ((inputLine = bufferedReader.readLine()) != null) {
                    stringBuffer.append(inputLine);
                }
                response = stringBuffer.toString();
            } else if (responseCode == 404) {
                response = "Company get department service is currently unavailable";
            } else {
                InputStream resStream = con.getErrorStream();
                InputStreamReader streamReader = new InputStreamReader(resStream);
                bufferedReader = new BufferedReader(streamReader);
                while ((inputLine = bufferedReader.readLine()) != null) {
                    stringBuffer.append(inputLine);
                }
                response = stringBuffer.toString();
//                response="Time out ";
            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            BELogger el = new BELogger(sw.toString());//&quot;
            el.log();
            response = e.getMessage();
        }
        BELogger responseLog = new BELogger("PLAYERRESPONSE", response);
        responseLog.log();
        return response;

    }
}
