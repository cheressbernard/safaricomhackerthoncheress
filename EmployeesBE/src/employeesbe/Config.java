/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeesbe;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import javax.annotation.PostConstruct;

/**
 *
 * @author Bernard
 */
public class Config {
    private static final String PROPS_FILE = "config.properties";

    //Declaring global variables
    public static String LOGFILE;
    public static String LOGS_PATH;

    @PostConstruct
    public static void LoadConfig() {
        try {
            Properties properties = new Properties();
            try (FileInputStream fileinput = new FileInputStream(PROPS_FILE)) {
                properties.load(fileinput);
            }

            LOGFILE = "/";
            LOGS_PATH = "/";
            System.out.println("Safaricom Config file loaded ...");
        } catch (IOException | NumberFormatException e) {
            System.out.println("Safaricom Config file loading failed ...");
            e.printStackTrace();
        }

    } 
}
