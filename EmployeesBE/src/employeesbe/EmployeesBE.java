/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeesbe;

import com.mysql.cj.xdevapi.JsonArray;
import database.DBFunctions;
import httpposter.HttpPoster;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Bernard
 */
public class EmployeesBE {

    Functions fct = new Functions();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Crete employee
        String id = "2", employee_name = "Sanju", employee_salary = "170750", employee_age = "63", profile_image = "";
        /*Uncomment this to create an employee and remember to change at least name value */
//        String emp_create_resp = CreateEmpoyee(id, employee_name, employee_salary, employee_age, profile_image);
//        System.out.println(emp_create_resp); id,name, age,  salary

       /*Uncomment this to update employee details */
        String update_resp = UpdateEmpoyee(id, employee_name, employee_age, employee_salary);
        System.out.println(update_resp);
         /*Uncomment this to getAllEmployers */
//        String update_resp= getAllPlayerNames();
//        System.out.println(update_resp);

             /*Uncomment this to retrieve employee */
//        String emp_records = RetrieveEmployees();
//        System.out.println(emp_records);
//

       /*Uncomment this to getAllEmployers */ /*From here*/
//        JSONArray json_arr = new JSONArray(emp_records);
//        JSONObject json_response = null;
//
//        DBFunctions dbf = new DBFunctions();
//        if (json_arr.length() > 1000) {
//            System.out.println("Records creater that 1000");
//            employee_name = "";
//            id = "";
//            profile_image = "";
//            employee_salary = "";
//            int age = 0;
//            for (int i = 0; i < json_arr.length(); i++) {
//                json_response = new JSONObject(json_arr.get(i));
//                age = json_response.getInt("employee_age");
//                id = json_response.getString("id");
//                employee_name = json_response.getString("employee_name");
//                employee_salary = json_response.getString("employee_salary");
//                profile_image = json_response.getString("profile_image");
//                if (age > 25) {
//                    dbf.CreateEmployee(id, employee_name, employee_age, employee_salary, profile_image);
//                }
//            }
//        } else {
//            System.out.println("Records less that 1000");
//        }
          /*to here*/
    }

    public static String CreateEmpoyee(String id, String employee_name, String employee_salary, String employee_age, String profile_image) {
        String employees_response = "";

        HttpPoster poster = new HttpPoster();
        employees_response = poster.CreteEmployee(id, employee_name, employee_salary, employee_age, profile_image);
        return employees_response;
    }

    public static String RetrieveEmployees() {
        String employees_response = "";
        HttpPoster poster = new HttpPoster();
        employees_response = poster.getEmployeeRecords();
        return employees_response;
    }

    public static String UpdateEmpoyee(String id, String name, String age, String salary) {
        String employees_response = "";
        HttpPoster poster = new HttpPoster();
        employees_response = poster.UpdateRecord(id, name, age, salary);
        return employees_response;
    }

    public static String getAllPlayerNames() {
        String player_response = "";
        String url = "https://footballpool.dataaccess.eu/info.wso?WSDL";
        String soapAction = "https://footballpool.dataaccess.eu/info.wso";
        String xml_request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:foot=\"https://footballpool.dataaccess.eu\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <foot:AllPlayerNames>\n"
                + "         <foot:bSelected>?</foot:bSelected>\n"
                + "      </foot:AllPlayerNames>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        HttpPoster poster = new HttpPoster();
        player_response = poster.HttpPost(xml_request, url, soapAction);
        return player_response;
    }

    public static String getOperationPlayerNames() {
        String player_response = "";
        String url = "https://footballpool.dataaccess.eu/info.wso?WSDL";// Config.PROVIDER_URL;
        String soapAction = "https://footballpool.dataaccess.eu/info.wso";
        String xml_request = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:foot=\"https://footballpool.dataaccess.eu\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <foot:AllPlayerNames>\n"
                + "         <foot:bSelected>?</foot:bSelected>\n"
                + "      </foot:AllPlayerNames>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>";
        HttpPoster poster = new HttpPoster();
        player_response = poster.HttpPost(xml_request, url, soapAction);
        return player_response;
    }

}
