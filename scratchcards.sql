-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2019 at 03:35 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scratchcards`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) NOT NULL,
  `employee_name` varchar(200) NOT NULL,
  `employee_salary` varchar(200) NOT NULL,
  `employee_age` varchar(20) NOT NULL,
  `profile_image` varchar(2000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` varchar(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `simcards`
--

CREATE TABLE `simcards` (
  `VOURCHERNO` varchar(100) NOT NULL,
  `SERIALNO` int(20) NOT NULL,
  `EXPIRYDATE` varchar(100) DEFAULT NULL,
  `AMOUNT` varchar(10) NOT NULL,
  `STATUS` varchar(15) NOT NULL DEFAULT 'ACTIVE',
  `DATECREATED` datetime DEFAULT CURRENT_TIMESTAMP,
  `DATEUPDATED` date DEFAULT NULL,
  `DELETED` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `simcards`
--

INSERT INTO `simcards` (`VOURCHERNO`, `SERIALNO`, `EXPIRYDATE`, `AMOUNT`, `STATUS`, `DATECREATED`, `DATEUPDATED`, `DELETED`) VALUES
('63191741529677126042130938755', 2, '2019-06-12 14:37:09', '100', 'ACTIVE', NULL, NULL, 0),
('813000707191380081069207470', 3, '2019-06-12 15:01:43', '100', 'ACTIVE', NULL, NULL, 0),
('21006943189489435011239425110', 4, '2019-06-12 16:27:15', '100', 'ACTIVE', NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `simcards`
--
ALTER TABLE `simcards`
  ADD PRIMARY KEY (`SERIALNO`),
  ADD UNIQUE KEY `VOURCHER_NO` (`VOURCHERNO`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `simcards`
--
ALTER TABLE `simcards`
  MODIFY `SERIALNO` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
